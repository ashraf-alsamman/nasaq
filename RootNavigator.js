import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator ,createAppContainer} from 'react-navigation';
 
  

 import Feed from './Feed'; 

const RootNavigator = createStackNavigator({

  Home: { screen: Feed }
  
}, {
    // no header
    headerMode: 'none',
});


 export default createAppContainer(RootNavigator);