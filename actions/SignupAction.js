import firebase from '../components/firebase';

export const SignupAction = ({email,password})=>{
 
return (dispatch)=>{
   
    dispatch({type:'loading_attempt'});

    firebase.auth().createUserWithEmailAndPassword(email,password).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;

      // ...
    });

    dispatch({type:'loading_done'});
  
}
 
}