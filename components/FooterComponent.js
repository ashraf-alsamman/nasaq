/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {StyleSheet} from 'react-native';
 
import {Button,Footer,FooterTab,Icon,Text} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import AutoHeightImage from 'react-native-auto-height-image';


type Props = {};
export default class FooterComponent extends Component<Props> {






 



  render() {
    return (

 
      <Footer  style={{ backgroundColor:'#fff', height:80}}>
      <FooterTab  style={{backgroundColor:'#fff',height:85,borderColor:'#fff'}}>

        <Button vertical  >
          <AutoHeightImage width={28} source={require('../resoures/settings.png')} />
        </Button>

        <Button vertical  >
          <AutoHeightImage width={28} source={require('../resoures/magnifyingGlass.png')} />    
        </Button>

        <Button vertical   >
              <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#3023AE', '#C86DD7']} 
              style={{ width: 60,  height: 60, borderRadius: 60/2, marginBottom:7, alignItems:'center',justifyContent:'center' }} >
              <AutoHeightImage width={28} source={require('../resoures/home.png')} />       
              </LinearGradient>

        </Button>

        <Button vertical  >
          <AutoHeightImage width={28} source={require('../resoures/heart.png')} />
        </Button>

        <Button vertical  >
          <AutoHeightImage width={28} source={require('../resoures/avatar.png')} />    
        </Button>

      </FooterTab>
    </Footer>


    );
  }

 
}

 
  const styles = StyleSheet.create({
    FooterBottunIcon: {
      color:'#000',
      fontSize: 30,
    },
 
    Footer: {
      backgroundColor:'#fff',
      fontSize: 9,
    },
 
  });
