 
import React, { Component } from 'react';
import { View,StyleSheet ,Image} from 'react-native';
import { Form,Item,Label,Input ,Spinner,StyleProvider ,Container,  thumbnail, CardItem,Grid,Col, Row, Thumbnail , Header, Title,Text, Content, Footer, FooterTab, Button, Left, Right, Body,Icon  } from 'native-base';
import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';
// load compomemts

import ImageBackgroundComponent from './components/ImageBackgroundComponent';
import HeaderComponent from './components/HeaderComponent';
import ButtonComponent from './components/ButtonComponent';
import styles from './components/styles';

import {connect} from 'react-redux';
import {AuthAction} from './actions';


//   const logo =  require('./assets/img/logo.png') ;


type Props = {};
 class Register extends Component<Props> {
  constructor(props) {
    super(props);
    

     this.state = { email: '',password:'' };
    // this.handleClick = this.handleClick.bind(this);
  }

  _login (){
    const  {email, password} = this.state;
    this.props.AuthAction({email,password});
  }
  _renderButton(){
if (this.props.loading){
  return           <Spinner color='blue' />

}
     return(     <Button    onPress={ this._login.bind(this)} ><Text>login</Text></Button>);



  }
  render() {
    return (
    
      <StyleProvider style={getTheme(platform)}>
     

      <Container   style={{flex: 1  }}>
      <ImageBackgroundComponent />
  <HeaderComponent back={true} RemoveNotifications={true}  navigation={this.props.navigation}>Login</HeaderComponent>
     
  <Content style={  styles.Content} >

  <View style={{justifyContent: 'center', alignItems: 'center',paddingVertical:55 }}>

 
 
</View> 
         
 
{/* //////////////// */}
<View >
  
 
    <Body  >

  <Input 
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
        onChangeText={(email) => this.setState({email})}
        value={this.state.email}
      />

            <Input 
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
        onChangeText={(password) => this.setState({password})}
        value={this.state.password}
      />
  <View style={{backgroundColor:'#fff', paddingVertical:10,paddingHorizontal:30}}>
  {this._renderButton()}
 </View>
 
    </Body>
  
  
           </View>                                                            
        </Content>


</Container>
</StyleProvider>


    );
  }


}

const mapStateToProps = state=> {
  return {
    error:state.AuthReducer.error,
    loading:state.AuthReducer.loading,
    user:state.AuthReducer.user
  }
} 

export default connect (mapStateToProps,{AuthAction})(Register)
