import React, { Component } from 'react';
import { View,TouchableOpacity} from 'react-native';
import { Spinner,StyleProvider ,Container, Grid,Col,Row, Text, Content,  Button, Left, Right, Body,Icon  } from 'native-base';
import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';
import firebase from './components/firebase';
import Dialog, {  DialogContent } from 'react-native-popup-dialog';
import FooterComponent from './components/FooterComponent';
import styles from './components/styles';
import Users from './Users';
import Post from './Post';
import AutoHeightImage from 'react-native-auto-height-image';
import LinearGradient from 'react-native-linear-gradient';
import RNFetchBlob from 'react-native-fetch-blob';
import ImagePicker from 'react-native-image-crop-picker';
 
import Lightbox from 'react-native-lightbox';


type Props = {};
 class Checkout extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = { data: [], dialogVisible: false,localData:null };
}
 
 

openPicker = () => {
  this.setState({ dialogVisible: false });
  //keep reference to original value
  const originalXMLHttpRequest = window.XMLHttpRequest;
     const Blob = RNFetchBlob.polyfill.Blob
    const fs = RNFetchBlob.fs
    window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
    window.Blob = Blob
    const uid = "12345"
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      mediaType: 'photo'
    }).then(image => {
 const imagePath = image.path ;
 let uploadBlob = null;
 const imageRef = firebase.storage().ref(uid).child("bbb.jpg")
 let mime = 'image/jpg'
 fs.readFile(imagePath, 'base64')
   .then((data) => { return   Blob.build(data, { type: `${mime};BASE64` }) })
   .then((blob) => {uploadBlob = blob ; return    imageRef.put(blob, { contentType: mime }); })
   .then(() => { uploadBlob.close();  window.XMLHttpRequest = originalXMLHttpRequest ; return imageRef.getDownloadURL(); })
   .then((url) => { let obj = {};  obj["loading"] = false; obj["dp"] = url;
     
   firebase.firestore().collection('images').add({
    link:url
  }).then((docRef) => { 

   this.setState({localData:url });

   }) .catch((error) => { return true;   }); 
 
  
  
  }) .catch((error) => {  console.log(error)  })}).catch((error) => {  console.log(error)})

}




    openCamera = () => {
 
      this.setState({ dialogVisible: false });
  //keep reference to original value
  const originalXMLHttpRequest = window.XMLHttpRequest;
    const Blob = RNFetchBlob.polyfill.Blob
    const fs = RNFetchBlob.fs
    window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
    window.Blob = Blob
    const uid = "12345"
ImagePicker.openCamera({width: 300,height: 400,cropping: true}).then(image => {
 const imagePath = image.path ;
 let uploadBlob = null;
 const imageRef = firebase.storage().ref(uid).child("bbb.jpg")
 let mime = 'image/jpg'
 fs.readFile(imagePath, 'base64')
   .then((data) => { return   Blob.build(data, { type: `${mime};BASE64` }) })
   .then((blob) => {uploadBlob = blob ; return    imageRef.put(blob, { contentType: mime }); })
   .then(() => { uploadBlob.close();  window.XMLHttpRequest = originalXMLHttpRequest ; return imageRef.getDownloadURL(); })
   .then((url) => { let obj = {};  obj["loading"] = false; obj["dp"] = url;
 
 
       firebase.firestore().collection('images').add({
         link:url
       }).then((docRef) => { 

        this.setState({localData:url });

        }) .catch((error) => { return true;   }); 
       
  })
   
   .catch((error) => {  console.log(error)  })}).catch((error) => {  console.log(error)})



 }









showlocalData() {

  if (this.state.localData == null) {
     return null;
} else {
   
      return(
          <Post image = {this.state.localData}></Post>
      );
  }
    
    
}
    


showData() {

  if (this.state.data.length == 0) {
    return <Spinner color="#2196f3" size='large'  ></Spinner>;
} else {
    return (

    this.state.data.map((link,key) => {
      return(
  
          <Post image = {link.link}></Post>
 
      );
  })
    
    
  ) ;  
    
   
}
}

  componentDidMount() {

    firebase.auth().signInAnonymously().catch(function(error) {  });
  
  firebase.firestore().collection("images").get().then((querySnapshot) => {
     querySnapshot.forEach((doc) => {
         var  joined = this.state.data.concat(doc.data());
         this.setState({ data: joined });
    });
    
}).catch(function(error) {
      Alert.alert('Error getting documents',  JSON.stringify(error)   );  //  this.setState({ data: error });
});


}


 
 
  render() {

    return (
    
<StyleProvider style={getTheme(platform)}>
    <Container  >
     
    <Dialog
    visible={this.state.dialogVisible}
    onTouchOutside={() => {
      this.setState({ dialogVisible: false });
    }}
 
  >
    <DialogContent style={{height:125,backgroundColor: 'transparent',padding:20}}>
  

          <Button iconLeft primary block  onPress={this.openCamera.bind(this)} >
            <Icon name='camera' />
            <Text>From camera</Text>
          </Button>

          <Button iconLeft primary syle={{marginTop:20}} block  onPress={this.openPicker} >
          <Icon name='photo-album' type='MaterialIcons' />
            <Text>From Gallery</Text>
          </Button>


    </DialogContent>
  </Dialog>



    <View   style={{  backgroundColor: '#fff'  ,marging:0,paddingTop:5 ,  padding:0 ,  height:60  }}  >
           
          <Left style={{flex:1 ,marging:0,padding:0,alignSelf: 'flex-start'}}>
<TouchableOpacity onPress={ () => {this.setState({ dialogVisible: true });}}>  
    <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#3023AE', '#C86DD7']} style={{  padding:14 ,marging:0 ,   width:60,  borderTopRightRadius:40,  borderBottomRightRadius:40  }} >
              <Icon name="camera"  type="FontAwesome" style={{  color:'#fff',  alignSelf: 'flex-end',  fontSize: 19}}   />
    </LinearGradient>
</TouchableOpacity>
          </Left>

  <Body  >
  <AutoHeightImage  width={160}  source={require('./resoures/new.png')} /> 
 </Body>   
          <Right style={{flex:1}}>
 
          </Right>

        </View>



 
        <Content style={  styles.Content} >


  <Users /> 
 

   {this.showlocalData()}  
 

 {this.showData()}






        </Content>





        <FooterComponent   />
 
</Container>
</StyleProvider>


    );
  }


}
 

 export default Checkout  ;
 