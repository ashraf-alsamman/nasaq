 
import React, { Component } from 'react';
import { View , ScrollView  } from 'react-native';
import { Grid,Col,Row, Text} from 'native-base';
 
import AutoHeightImage from 'react-native-auto-height-image';
import LinearGradient from 'react-native-linear-gradient';
import Lightbox from 'react-native-lightbox';
import { Dimensions } from 'react-native'

 
 

type Props = {};
 class Post extends Component<Props> {
 
 
  render() {

    return (

<View>
        <Grid style={{paddingHorizontal:20,marginBottom:20,paddingTop:20,marginTop:20, borderTopWidth:1,borderColor:'#BFBFBF'}}>

        <Col style={{width:60}}>
                <LinearGradient colors={['#3023AE', '#C86DD7']} style={{marginHorizontal:4, width:40,  height: 40, borderRadius: 40/2, marginBottom:7, alignItems:'center',justifyContent:'center' }} >
                  <AutoHeightImage width={35} source={require('./resoures/ovalCopy.png')}  style={{}}/>
                </LinearGradient> 
        </Col>
        <Col >
          <Text style={{   fontSize:18}}>Glen Lyons</Text> 
          <Row>

          <Col> 
          <Text style={{ color:'#BFBFBF', fontSize:15}}>Saudi arabia</Text> 
          </Col> 

          <Col>
          <Text  style={{ color:'#BFBFBF', marging:0,padding:0,alignSelf: 'flex-end',fontSize:15}}>23 hours ago</Text>
          </Col> 

          </Row>

        </Col> 

        </Grid>

         <Lightbox >
            <AutoHeightImage width={Dimensions.get('window').width} source={{uri:this.props.image}} />       
         </Lightbox>


</View>

    );
  }


}

  

 export default Post ;
 